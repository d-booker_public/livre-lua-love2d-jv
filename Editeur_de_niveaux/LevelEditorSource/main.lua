-- Utilisation du script classic
Object = require "classic"
-- Inclusions
require "settings"
require "tile"

-- Chargement du tileset
tileset = love.graphics.newImage("tileset.png")
-- Id de la tile sélectionnée. Défaut = 1
tileId = 1
-- Tableau de tiles (objet Tile) à dessiner
tiles = {}
quads = {} -- Liste quads (textures des tiles)
-- Menu de droite
menu = love.graphics.newImage("menu.png")
-- Tableau de tableaux contenant les id
-- Utilisé pour l'export
tilesArray = {
	{},{},{},{}, {},{},{},{},
	{},{},{},{}, {},{},{},{},
	{},{},{},{}, {},{},{},{},
	{},{},{},{}, {},{},{},{}
}

function love.load()
	-- Nous avons 42 tiles donc on crée 42 quads 
	for i=0, 41 do
		table.insert(quads, love.graphics.newQuad(i * tileSize, 0, tileSize, tileSize, tileset:getDimensions()))
	end
end

function love.draw() 

	for i, t in ipairs(tiles) do 
		t:draw()
	end

	DrawGrid() -- Dessiner la grille
	
	love.graphics.draw(menu, 1025,0)
	love.graphics.draw(tileset, 0, 800) -- Tileset
	-- Afficher les instructions
	love.graphics.print(string.upper("Cliquez sur une tuile ci-dessous pour la sélectionner puis cliquez sur la map pour créer. Appuyez sur C pour copier votre map, L pour charger la map de base."), 10, 776)

end

function love.update(dt)
	if love.mouse.isUp(1) then -- Si on clique
		-- Récupération de la position du curseur en pixels
		local x = love.mouse.getX()
		local y = love.mouse.getY()
		-- Conversion pixer vers cases de la grille
		mousePosX = math.floor((x/32)+1)
        	mousePosY = math.floor((y/32)+1)

    	-- Si on clique sur l'image du tileset 
    	if mousePosY == 26 then -- on sélectionne une tuile
    		tileId = mousePosX
    		print(tileId) -- Pour débug
    	-- Si on clique sur la grille
    	elseif mousePosY < 25 and mousePosX < 33 then -- dessiner tuile
    		-- On ajoute l'id dans le tableau pour l'export
    		tilesArray[mousePosX][mousePosY] = tileId
    		-- On ajoute dans le tableau la tile créée
    		table.insert(tiles, Tile(quads[tileId], mousePosX, mousePosY, tileId))
    	end

    	-- Clic menu droite
    	if x > 1048 and x < 1340 then -- Si on clique sur la droite
    		if y > 137 and y < 187 then -- Ouvrir tuto
    			love.system.openURL("http://anthony-cardinale.fr/livres/love/tuto-editeur-love.mp4")
    		elseif y > 320 and y < 460 then -- Charger tpl 1
    			LoadTpl(1)
    		elseif y > 468 and y < 606 then -- Charger tpl 2
    			LoadTpl(2)
    		elseif y > 616 and y < 753 then -- Charger tpl 3
    			LoadTpl(3)
    		end
    	end
	end
end

function love.keypressed(key)
	if key == "c" then
		SaveMap()
	end
	if key == "l" then
		LoadTpl(4)
	end
end

function DrawGrid()
	-- Lignes verticales
	for x=tileSize, mapW*tileSize, tileSize do
		love.graphics.line(x, 0, x, mapH*tileSize)
	end
	-- Lignes horizontales
	for y=tileSize, mapH*tileSize, tileSize do
		love.graphics.line(0, y, mapW*tileSize, y)
	end
end


function LoadTpl(id)
	local map = {}

	if id == 1 then
		map = template1
	elseif id == 2 then
		map = template2
	elseif id == 3 then
		map = template3
	elseif id == 4 then
		map = default
	end

	cpt = 1
	for y=1, mapH, 1 do
		for x=1, mapW, 1 do
			-- Tableau pour l'export
			tilesArray[x][y] = map[cpt]
			table.insert(tiles, Tile(quads[map[cpt]], x, y, map[cpt]))
			cpt = cpt + 1
		end
	end
end

function SaveMap()
	-- Création de la chaîne de caractères format Lua
	saveString = "{"
	for y=1, mapH, 1 do
		for x=1, mapW, 1 do
			saveString = saveString .. tostring(tilesArray[x][y]) .. ","
		end
	end
	saveString = saveString:sub(1, -2) -- On supprime la dernière virgule
	saveString = saveString .. "}" 
	print(saveString) 
	love.system.setClipboardText( saveString ) -- Copie dans presse-papier
	print("Map copiée !")
end
