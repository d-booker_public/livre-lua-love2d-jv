-- Exemple de tableau
tab = {4,2,6,8}

function love.draw()
	-- Nos comparaisons
	if tab[1] > tab[2] then
		love.graphics.print(tab[1] .. " > " .. tab[2], 20, 40)
	end
	if tab[1] > tab[3] then
		love.graphics.print(tab[1] .. " > " .. tab[3], 20, 80)
	end
	if tab[1] > tab[4] then
		love.graphics.print(tab[1] .. " > " .. tab[4], 20, 120)
	end
end	