-- Exemple de tableau
tab = {4,2,3,6,8,5,8,3,7,1}

function love.draw()
	-- On boucle
	for i = 1, 10 do
		-- Si le chiffre actuel est plus grand que le premier chiffre 
		if tab[1] < tab[i] then
			-- On affiche à l'écran
			love.graphics.print(tab[1] .. " < " .. tab[i], 20, 20)
			-- On arrête la boucle avec break
			break
		end
	end
end	