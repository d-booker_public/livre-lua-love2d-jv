
function love.draw()
   	CreerEtoiles()

	love.graphics.setColor(0.15, 0.4, 0.9)
	love.graphics.circle("fill", 500, 350, 200)
	love.graphics.setColor(0.15, 0.9, 0.2)
	local liste1 = {338,320,410,200,455,250,396,404,321,352}
	love.graphics.polygon('fill', liste1)
	local liste2 = {500,300,550,250,600,300,550,358,452,400}
	love.graphics.polygon('fill', liste2)
	local liste3 = {450,450,550,500,650,400,550,450}
	love.graphics.polygon('fill', liste3)
	local liste4 = {450,165,592,174,570,222}
	love.graphics.polygon('fill', liste4)
end	

function CreerEtoiles()
	love.graphics.setColor(1, 1, 1, 0.85)
	love.graphics.points(etoiles)
end

function love.load()
   etoiles = {}
   for i=1, 100 do
   	-- Random génère un nombre aléatoire compris entre 2 valeurs
      local x = love.math.random(5, 1020) 
      local y = love.math.random(5, 760)
      etoiles[i] = {x, y}
   end
end