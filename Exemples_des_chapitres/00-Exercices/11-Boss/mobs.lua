-- Paramètres du mob 1
mob1 = {}
mob1.yline = 2*tileSize
mob1.sheet = love.graphics.newImage("mob.png")
mob1.sprite = love.graphics.newQuad(0, mob1.yline, tileSize, tileSize, mob1.sheet:getDimensions())
mob1.x = 11*tileSize
mob1.y = 11*tileSize
mob1.speed = 96
mob1.goRight = true
mob1.detectDistance = 320

-- Vous pouvez créer d'autres mobs / boss
-- Exemple boss
boss = {}
boss.yline = 2*tileSize
boss.sheet = love.graphics.newImage("mob.png") -- Penser à changer l'image
-- Il est possible de créer une image plus grande pour le boss
boss.sprite = love.graphics.newQuad(0, boss.yline, tileSize, tileSize, boss.sheet:getDimensions())
boss.x = 11*tileSize
boss.y = 11*tileSize
boss.speed = 128 -- Plus rapide
boss.goRight = true
boss.detectDistance = 512 -- Détecte de plus loin
boss.vie = 15 -- Il faut lui tirer 15 fois dessus et pas 1 !


-- Fonction qui retourne la distance entre 2 points
-- Nous l'utiliserons pour tester la distance entre
-- Le mob et le joueur
function Distance ( x1, y1, x2, y2 )
  local dx = x1 - x2
  local dy = y1 - y2
  return math.sqrt ( dx * dx + dy * dy )
end
