nbClics = 0
posX = 5000
posY = 5000

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)

	love.graphics.print("Nombre de clics = " .. nbClics, 20, 20)
	love.graphics.circle("fill", posX, posY, 5+nbClics)
end	

function love.mousepressed(x, y, button)
    if button == 1 then
        posY = y
        posX = x
    end
end
 
function love.mousereleased(x, y, button)
    if button == 1 then
        nbClics = nbClics + 1
    end
end