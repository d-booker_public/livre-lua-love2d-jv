posX = 100
posY = 100

function love.load()
   logo = love.graphics.newImage("logo.png")
end

function love.draw()
	love.graphics.draw(logo, posX, posY)
end	

function love.keypressed(key, unicode)
    if key == "escape" then
    	love.event.quit()
   elseif key == "right" then
    	posX = posX + 50
    elseif key == "left" then
    	posX = posX - 50
    elseif key == "down" then
    	posY = posY + 50
    elseif key == "up" then
    	posY = posY - 50
   end
end
