-- Tableau avec les infos du personnage
perso = {}
perso.img = love.graphics.newImage("assets/love-fish.png")
perso.x = 400
perso.y = 400
perso.r = math.rad(0)
perso.sx = -1
perso.sy = 1
perso.size = 160

function love.load()
	bg = love.graphics.newImage("assets/bg.png")
end

function love.draw()
	-- Affichage de l'image de fond
	love.graphics.draw(bg, 0, 0)
	-- Personage
	love.graphics.draw(perso.img, perso.x, perso.y, perso.r, perso.sx, perso.sy, perso.size/2, perso.size/2)
end	

function love.update(dt)
	if love.keyboard.isDown("right") then
		perso.sx = 1
	elseif love.keyboard.isDown("left") then
		-- J'inverse le scale x pour faire une symétrie de l'image
		perso.sx = -1
	end
end	
