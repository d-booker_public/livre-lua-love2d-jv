require "collision"

-- Tableau avec les infos du personnage
perso = {}
perso.img = love.graphics.newImage("assets/love-fish.png")
perso.x = 128
perso.y = 256
perso.r = math.rad(0)
perso.sx = 0.7
perso.sy = 0.7
perso.size = 160
perso.speed = 300

mine = {}
mine.img = love.graphics.newImage("assets/mine-big.png")
mine.x = 350
mine.y = 400
mine.size = 69

nbMines = 0

function love.load()
	-- Pour avoir de l'aléatoire réel
	math.randomseed(os.time())
	-- Chargement de l'image
	bg = love.graphics.newImage("assets/bg.png")
	mine.x = love.math.random(100, 900)
	mine.y = love.math.random(100, 700)
end

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)

	-- Affichage de l'image de fond
	love.graphics.draw(bg, 0, 0)

	love.graphics.print("Mines : " .. nbMines, 20, 20)
	-- Mine
	love.graphics.draw(mine.img, mine.x, mine.y)
	-- Personage
	if nbMines >= 5 then
		love.graphics.setColor(0.9,0,0)
	end
	love.graphics.draw(perso.img, perso.x, perso.y, perso.r, perso.sx, perso.sy, perso.size/2, perso.size/2)
	love.graphics.setColor(1,1,1)
end	

function love.update(dt)
	if love.keyboard.isDown("up") then
		perso.y = perso.y - perso.speed * dt
	elseif love.keyboard.isDown("right") and perso.x < love.graphics.getWidth() then
		perso.x = perso.x + perso.speed * dt
	elseif love.keyboard.isDown("down") and perso.y < love.graphics.getHeight() then
		perso.y = perso.y + perso.speed * dt
	elseif love.keyboard.isDown("left") and perso.x > 0 then
		perso.x = perso.x - perso.speed * dt
	end

	TraverserEcran()

	if checkCollision(perso.x-80, perso.y-80, perso.size, perso.size,
					  mine.x, mine.y, mine.size, mine.size) then
		-- Il y a collision
		DeplacerMine()
	end
end	

function DeplacerMine()
	mine.x = love.math.random(100, 900)
	mine.y = love.math.random(100, 700)
	nbMines = nbMines +1
	if nbMines >= 10 then
		love.event.quit()
	end
end

function TraverserEcran()
	if perso.x < -1 then
		perso.x = love.graphics.getWidth() - 1
	elseif perso.x > love.graphics.getWidth() then
		perso.x = 1
	elseif perso.y < -1 then
		perso.y = love.graphics.getHeight() - 1
	elseif perso.y > love.graphics.getHeight() then
		perso.y = 1
	end
end