require "settings"

tileset = love.graphics.newImage("tileset.png")

tile3  = love.graphics.newQuad(tileSize*2,  0, tileSize, tileSize, tileset:getDimensions())
tile6  = love.graphics.newQuad(tileSize*6,  0, tileSize, tileSize, tileset:getDimensions())

function love.draw()
	for x=0, 1024, 32 do 
		for y=0, 768, 32 do 
			if x == 512 then
				love.graphics.draw(tileset, tile6, x, y)
			else
				love.graphics.draw(tileset, tile3, x, y)
			end	
		end	
	end
end	
