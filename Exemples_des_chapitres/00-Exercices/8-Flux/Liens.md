Dépôt officiel de Flux : [https://github.com/rxi/flux](https://github.com/rxi/flux)

### Pour modifier la transition de votre tween
`:ease("linear")`
Remplacer "linear" par la transition de votre choix

Liste des transitions :
```
linear quadin quadout quadinout cubicin cubicout cubicinout quartin quartout quartinout quintin quintout quintinout expoin expoout expoinout sinein sineout sineinout circin circout circinout backin backout backinout elasticin elasticout elasticinout
```


### Déclencher des fonctions lors d'un tween
`:onstart(votreFonction)`
Pour déclencher la fonction votreFonction au début d'une transition


`:oncomplete(votreFonction2)`
Pour déclencher votreFonction2 à la fin d'une transition


### Vous pouvez mettre fin à un tween prématurément si besoin
`:stop()`
Pour arrêter un tween




... Et bien plus sur la doc
