-- Fonction standard de love qui affiche des choses à l'écran
function love.draw()
	-- Fonction permettant d'afficher un texte à l'écran à des coordonnées
	love.graphics.print("Hello World!", 350, 300)
end