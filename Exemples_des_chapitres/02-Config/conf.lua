-- Fonction de configuration de LÖVE
function love.conf(w)
	-- On donne une taille en pixel à la fenêtre
	w.window.width = 1024 -- Largeur
	w.window.height = 768 -- Hauteur
	-- On donne un titre à la fenêtre
	w.title = "Titre de mon jeu"

	-- Les autres paramètres
	w.version = "11.3" -- La version de LÖVE pour lequel le jeu a été prévu
	w.console = false -- Afficher la console ?
	w.window.icon = "dbk.ico" -- L'icône de votre fenêtre
	w.window.borderless = false -- Pour supprimer les bordures de la fenêtre
	w.window.resizable = false -- La fenêtre peut-elle être redimensionnée ?
	w.window.fullscreen = false -- Le jeu doit-il être affiché en plein écran ?
	w.window.msaa = 0 -- Pour activer l'antialiasing. Vous pouvez mettre une valeur de 2 ou 4 pour lisser les arêtes
	w.window.display = 1 -- Sur quel écran doit-on afficher la fenêtre du jeu ?
	w.window.x = nil -- Position x de la fenêtre sur votre écran
	w.window.y = nil -- Idem en y
end