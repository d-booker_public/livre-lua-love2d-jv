-- Fonction standard de love qui affiche des choses à l'écran
function love.draw()
	-- Chargement d'une police d'écriture
	font = love.graphics.newFont("PressStart2P.ttf", 40)
	-- On utilise cette police d'écriture
	love.graphics.setFont(font)
	-- Fonction permettant d'afficher un texte à l'écran à des coordonnées
	love.graphics.print("Hello World!", 280, 320)
	-- On affiche un autre texte plus petit
	-- Modification de la taille du texte par défaut et utilisation de la police par défaut
	love.graphics.setNewFont(20)
	love.graphics.print("Un sous titre plus petit", 390, 380)
end