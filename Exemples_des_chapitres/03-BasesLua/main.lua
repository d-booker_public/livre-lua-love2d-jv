nom = "Joueur 1"
-- Création de la variable vie 
vie = 3 -- On donne la valeur 3
-- Exemple d'opérations 
vie = vie -1 -- vie = 2
vie = vie * 2 -- vie = 4

note1 = 12.5
note2 = 14
note3 = 9.75

moyenne = (note1+note2+note3)/3

-- Création d'un tableau pour J2
monTableau = {5, 9, 3, 7, 0}

-- Exemple d'utilisation d'un tableau
joueur = {}
joueur.vie = 5
joueur.nom = "Joueur 2"
joueur.force = 150

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 28)
	love.graphics.setFont(font)
	-- On affiche le nom et la vie à l'écran
	love.graphics.print(nom, 20, 20)
	love.graphics.print("Vie : " .. tostring(vie), 800, 20)
	love.graphics.print("Moyenne : " .. tostring(moyenne), 20, 50)
	love.graphics.print(monTableau[1], 20, 80)

	love.graphics.print(joueur.nom .. " a " .. joueur.force .. " points de force", 20, 160)
end	