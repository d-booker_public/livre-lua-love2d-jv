-- Fonction de configuration de LÖVE
function love.conf(w)
	-- On donne une taille en pixel à la fenêtre
	w.window.width = 1024 -- Largeur
	w.window.height = 768 -- Hauteur
	-- On donne un titre à la fenêtre
	w.title = "LIVRE LÖVE 2D - CARDINALE Anthony"
end