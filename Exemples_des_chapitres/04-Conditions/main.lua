argent = 2

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 28)
	love.graphics.setFont(font)
	
	if argent > 5 then
		love.graphics.print("J'ai plus que 5 pièces :)", 20, 20)
	else
		love.graphics.print("J'ai moins de 5 pièces :(", 20, 20)
	end
end	