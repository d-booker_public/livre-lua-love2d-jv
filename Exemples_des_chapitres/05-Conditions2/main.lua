argent = 10
stock = 2
prix = 6

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)
	
	-- Le joueur a assez d'argent
	-- ET
	-- Le vendeur a du stock
	if argent > prix and stock > 0 then
		love.graphics.print("Argent > prix et stock > 0, parfait !", 20, 20)
	end

	-- Plusieurs tests à la suite 
	if prix > 0 and prix < 3 then
		love.graphics.print("C'est pas cher !", 20, 60)
	elseif prix > 2 and prix < 6 then
		love.graphics.print("C'est abordable.", 20, 60)
	elseif prix > 5 and prix < 9 then
		love.graphics.print("C'est pas donné !", 20, 60)
	elseif prix > 10 then
		love.graphics.print("C'est trop cher !", 20, 60)
	end
end	