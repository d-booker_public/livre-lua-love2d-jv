uneCondition = true
compteur = 0

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)
	
	-- Boucle For
	-- Pour i=1, tant que i<10, i=i+1
	for i = 1, 10 do
	  love.graphics.print("i = " .. i, 20, 30 * i)
	end

	-- Boucle while
	-- Tant que uneCondition==true fais...
	while uneCondition do
		compteur = compteur + 1
		if compteur >= 10 then uneCondition = false end
	end
end	