-- Les fonctions propres à LÖVE
-- Pour l'initialisation
function love.load()
	resultat = Additionner(6, 4)
end	

-- Pour dessiner à l'écran
function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)
	love.graphics.print(resultat, 20, 20)
end	

-- Tourne en boucle (60 fois par seconde)
function love.update(dt)

end	

-- Teste les inputs 
function love.keypressed(key)
	
end

-- Fonctions personnelles
function Additionner(a, b)
	return a + b
end