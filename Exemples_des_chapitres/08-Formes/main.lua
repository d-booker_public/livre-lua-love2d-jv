rect1 = {}
rect1.posX = 200
rect1.posY = 100
rect1.width = 180
rect1.height = 120

function love.draw()
	love.graphics.setBackgroundColor(0.8, 0.8, 0.9)
	love.graphics.setColor(1, 0, 0)
	love.graphics.rectangle("line", rect1.posX, rect1.posY, rect1.width, rect1.height, 10, 10, 3)
	love.graphics.setColor(0, 1, 0)
	love.graphics.circle("fill", 350, 400, 90)
	local listePoints = {450, 100, 650, 100, 500, 250}
	love.graphics.setColor(0, 0, 1)
	love.graphics.polygon('fill', listePoints)
end	

function love.keypressed(key)
	if key == "c" then
        love.graphics.captureScreenshot("MonImage.png")
    end
end