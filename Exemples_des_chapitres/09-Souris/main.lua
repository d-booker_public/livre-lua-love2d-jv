
function love.load()
    cursor = love.mouse.newCursor("logo.png")
end

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)
	local x, y = love.mouse.getPosition()
	love.graphics.print(x..", "..y, 20, 20)
end	

function love.update(dt)
	if love.mouse.isDown(1) then -- Si clic gauche
		love.graphics.setBackgroundColor(1,1,1)
	end
end	

function love.mousepressed(x, y, button)
	-- On affiche un curseur personnalisé
    if button == 1 then
        love.mouse.setCursor(cursor)
    end
end
 
function love.mousereleased(x, y, button)
	-- On remet le curseur par défaut
    if button == 1 then
        love.mouse.setCursor()
    end
end