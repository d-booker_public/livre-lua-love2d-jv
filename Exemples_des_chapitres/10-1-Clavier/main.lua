txt = ""
nbSpace = 0

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)
	
	love.graphics.print(nbSpace, 20, 20)
	love.graphics.print(txt, 20, 50)
end	

function love.keypressed(key, unicode)
	-- Si on appuie sur échap
    if key == "escape" then
    	-- On quitte le jeu
    	love.event.quit()
    -- Si on appuie sur espace
   elseif key == "space" then
    	nbSpace = nbSpace + 1
    	love.graphics.setBackgroundColor(0.4,0.4,0.4,0.4)
   end
end

function love.keyreleased(key)
   if key == "space" then
      love.graphics.setBackgroundColor(0,0,0,1)
   end
end

function love.update(dt)
	if love.keyboard.isDown("up") then
		txt = "HAUT"
	elseif love.keyboard.isDown("right") then
		txt = "DROITE"
	elseif love.keyboard.isDown("down") then
		txt = "BAS"
	elseif love.keyboard.isDown("left") then
		txt = "GAUCHE"
	end
end	
