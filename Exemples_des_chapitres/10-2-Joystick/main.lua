-- Utilisation d'une manette type xBox
function love.load()
	x = 0
	y = 0
	btn = ""
	p1joystick = nil
end

function love.joystickadded(joystick)
    p1joystick = joystick
end

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)
	-- On affiche les informations du joystick
	love.graphics.print("x : "..x.." ; y : "..y, 20, 80)
	love.graphics.print(btn, 20, 40)
end	

function love.update(dt)
	-- Vérif si joystick connecté
    if p1joystick ~= nil then
        -- getGamepadAxis retourne une valeur comprise entre -1 and 1.
        -- retourne 0 si pas touché
 		-- On récupère les infos x et y du joystick
        x = x + p1joystick:getGamepadAxis("leftx")
        y = y + p1joystick:getGamepadAxis("lefty")
        -- On récupère les infos des 4 btns du joystick
        if p1joystick:isGamepadDown('a') then
	        btn = "A appuyé !"
	    end
	    if p1joystick:isGamepadDown('b') then
	        btn = "B appuyé !"
	    end
	    if p1joystick:isGamepadDown('x') then
	        btn = "X appuyé !"
	    end
	    if p1joystick:isGamepadDown('y') then
	        btn = "Y appuyé !"
	    end
	    -- On récupère les infos des flèches de la manette
	    -- (Croix directionnelle)
	    if p1joystick:isGamepadDown('dpleft') then
	        btn = "Flèche gauche appuyée !"
	    end
	    if p1joystick:isGamepadDown('dpright') then
	        btn = "Flèche droite appuyée !"
	    end
	    if p1joystick:isGamepadDown('dpup') then
	        btn = "Flèche haut appuyée !"
	    end
	    if p1joystick:isGamepadDown('dpdown') then
	        btn = "Flèche bas appuyée !"
	    end
	    -- Gachettes
	    if p1joystick:isGamepadDown('leftshoulder') then
	        btn = "leftshoulder appuyé !"
	    end
	    if p1joystick:isGamepadDown('rightshoulder') then
	        btn = "rightshoulder appuyé !"
	    end
    end
end	
