-- Tableau avec les infos du personnage
perso = {}
perso.img = love.graphics.newImage("assets/love-fish.png")
perso.x = 128
perso.y = 256
perso.r = math.rad(0)
perso.sx = 0.7
perso.sy = 0.7
perso.size = 160
perso.speed = 200

mine = {}
mine.img = love.graphics.newImage("assets/mine-big.png")
mine.x = 350
mine.y = 400

function love.load()
	-- Pour avoir de l'aléatoire réel
	math.randomseed(os.time())
	-- Chargement de l'image
	bg = love.graphics.newImage("assets/bg.png")
	mine.x = love.math.random(100, 900)
	mine.y = love.math.random(100, 700)
end

function love.draw()
	font = love.graphics.newFont("PressStart2P.ttf", 22)
	love.graphics.setFont(font)
	-- Affichage de l'image de fond
	love.graphics.draw(bg, 0, 0)
	-- Personage
	love.graphics.draw(perso.img, perso.x, perso.y, perso.r, perso.sx, perso.sy, perso.size/2, perso.size/2)
	-- Mine
	love.graphics.draw(mine.img, mine.x, mine.y)
end	

function love.update(dt)
	if love.keyboard.isDown("up") then
		perso.y = perso.y - perso.speed * dt
	elseif love.keyboard.isDown("right") then
		perso.x = perso.x + perso.speed * dt
	elseif love.keyboard.isDown("down") then
		perso.y = perso.y + perso.speed * dt
	elseif love.keyboard.isDown("left") then
		perso.x = perso.x - perso.speed * dt
	end
end	

-- coins bords / collisions

-- function love.keypressed(key)
-- 	if key == "right" then
-- 		perso.x = perso.x + 64
-- 	elseif key == "left" then
-- 		perso.x = perso.x - 64
-- 	elseif key == "up" then
-- 		perso.y = perso.y - 64
-- 	elseif key == "down" then
-- 		perso.y = perso.y + 64
-- 	end
-- end