require "settings" -- Nos options
require "allmaps" -- tous les fichiers map

tileset = love.graphics.newImage("tileset.png")
quads = {} -- Liste des tiles / quads

function love.load()
	-- Nous avons 42 tiles donc on crée 42 quads 
	for i=0, 41 do
		table.insert(quads, love.graphics.newQuad(i * tileSize, 0, tileSize, tileSize, tileset:getDimensions()))
	end
end

function love.draw()
	DrawMap(map1)
end	

-- On dessine la map passée en paramètre
function DrawMap(map)
	cpt = 1 
	for y=1, love.graphics.getHeight(), 32 do
		for x=1, love.graphics.getWidth(), 32 do
			love.graphics.draw(tileset, quads[map[cpt]], x, y)
			cpt = cpt + 1
		end
	end
end