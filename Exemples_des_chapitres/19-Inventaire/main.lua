require "collision" -- Pour accéder à la fonction de collision
require "settings" -- Nos options
require "inventaire" -- Inventaire
require "gui" -- GUI
require "allmaps" -- tous les fichiers map
flux = require "flux" -- Outil flux pour les tweens

tileset = love.graphics.newImage("tileset.png")
quads = {} -- Liste des tiles / quads

-- Tableau de tableaux stockant les cases sous la forme case[x][y]
tilesArray = {
	{},{},{},{}, {},{},{},{},
	{},{},{},{}, {},{},{},{},
	{},{},{},{}, {},{},{},{},
	{},{},{},{}, {},{},{},{}
}

-- Variables pour le personnage
player = {}
-- Pos du joueur à l'écran
player.posX = 128
player.posY = 352
-- Vitesse de déplacement
player.speed = 32
-- Feuille de sprite
player.sprite_sheet = love.graphics.newImage("hero.png")
-- Quelle partie de l'image devons nous afficher ?
player.yline = 32
player.xline = 0
-- Quad
player.sprite = love.graphics.newQuad(player.xline, player.yline, tileSize, tileSize, 
	player.sprite_sheet:getDimensions())
-- Variables pour gérer l'animation du perso
player.anim_timer = 0.1
player.frame = 0
player.max_frame = 2


function love.load()
	-- Nous avons 42 tiles donc on crée 42 quads 
	for i=0, 41 do
		table.insert(quads, love.graphics.newQuad(i * tileSize, 0, tileSize, tileSize, tileset:getDimensions()))
	end
	love.graphics.setFont(love.graphics.newFont(32))
end

function love.draw()
	if mapActuelle == "Map1" then
		DrawMap(map1.grid)
		map1.draw() -- Dessine les éléments de la map
	elseif mapActuelle == "Map2" then
		DrawMap(map2.grid)
	elseif mapActuelle == "Menu" then
		love.graphics.print("Press space to play!",330,280)
	end

	if mapActuelle ~= "Menu" then -- Si pas sur menu
		love.graphics.draw(player.sprite_sheet, player.sprite, player.posX, player.posY, 0, 1, 1, tileSize/2, tileSize/2)
		-- GUI / UI
		DrawGui()
	end
end	

function love.update(dt)
	flux.update(dt) -- Update de flux
	InputManager(dt) -- Gestion inputs
	AnimPlayer(dt) -- Fonction d'animation du perso

	if mapActuelle == "Map1" then
		map1.update(dt)
	elseif mapActuelle == "Map2" then
		map2.update(dt)
	elseif mapActuelle == "Menu" then
		if love.keyboard.isDown("space") then
			mapActuelle = "Map1"
		end
	end
end

-- On dessine la map passée en paramètre
function DrawMap(map)
	cpt = 1 
	for y=1, love.graphics.getHeight(), tileSize do
		for x=1, love.graphics.getWidth(), tileSize do
			-- On passe des pixels à ligne/colonne
			caseX = math.floor((x/tileSize)+1)
    		caseY = math.floor((y/tileSize)+1)
    		-- On indique dans le tableau la valeur de la tile à sa place
			tilesArray[caseX][caseY] = map[cpt]
			love.graphics.draw(tileset, quads[map[cpt]], x, y)
			cpt = cpt + 1
		end
	end
end

function AnimPlayer(dt)
	player.anim_timer = player.anim_timer - dt
	if player.anim_timer <= 0 then
		player.anim_timer = 0.1
		player.frame = player.frame + 1
		if player.frame > player.max_frame then player.frame = 0 end
		player.xline = tileSize * player.frame
		player.sprite:setViewport(player.xline, player.yline, tileSize, tileSize)
	end
end

function InputManager(dt)
	-- Si on appuie sur la flèche du haut
	if love.keyboard.isDown("up") then
		-- On diminue Y et on joue la ligne 4 de l'animation
		MovePlayer(player.posX, player.posY - player.speed, 3)
	elseif love.keyboard.isDown("down") then
		MovePlayer(player.posX, player.posY + player.speed, 0)
	elseif love.keyboard.isDown("left") then
		MovePlayer(player.posX - player.speed, player.posY, 1)
	elseif love.keyboard.isDown("right") then
		MovePlayer(player.posX + player.speed, player.posY, 2)
	else
		player.max_frame = 0 -- On arrête l'anim si on ne bouge pas
	end
end

function MovePlayer(x,y,yline)
	player.yline = tileSize * yline -- On affiche la bonne anim
	player.max_frame = 2 -- On joue l'anim
	-- On calcule la position par case au lieu de pixels
	caseX = math.floor((x/tileSize)+1)
    caseY = math.floor((y/tileSize)+1)
    -- Si on se déplace sur la grille
    if caseX > 0 and caseY > 0 and caseX < mapW and caseY < mapH then
	    -- Si la case cible est "marchable"
	    if tilesArray[caseX][caseY] < 9 or tilesArray[caseX][caseY] == 36 then
	    	-- On déplace le joueur
			tween = flux.to(player, 0.3, { posX = x, posY = y })
		end
	end
end
