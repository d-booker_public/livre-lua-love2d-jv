-- Paramètres du mob 1
mob1 = {}
mob1.yline = 2*tileSize
mob1.sheet = love.graphics.newImage("mob.png")
mob1.sprite = love.graphics.newQuad(0, mob1.yline, tileSize, tileSize, mob1.sheet:getDimensions())
mob1.x = 11*tileSize
mob1.y = 11*tileSize
mob1.speed = 96
mob1.goRight = true
mob1.detectDistance = 320

-- Vous pouvez créer d'autres mobs / boss

-- Fonction qui retourne la distance entre 2 points
-- Nous l'utiliserons pour tester la distance entre
-- Le mob et le joueur
function Distance ( x1, y1, x2, y2 )
  local dx = x1 - x2
  local dy = y1 - y2
  return math.sqrt ( dx * dx + dy * dy )
end
