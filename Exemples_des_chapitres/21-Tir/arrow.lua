Arrow = Object:extend()

function Arrow:new(x, y, r)
	self.img = love.graphics.newImage("arrow.png") -- Image
	self.x = x -- Position sur l'axe x
	self.y = y -- idem axe Y
	self.r = r -- Rotation en radians
	self.speed = 400 -- Vitesse du projectile
	self.w = self.img:getWidth() -- Largeur
	self.h = self.img:getHeight() -- Hauteur
end

function Arrow:update(dt)
	-- On propulse la flèche dans la bonne direction
	if self.r == math.rad(-90) then -- droite
		self.x = self.x + self.speed * dt
	elseif self.r == math.rad(90) then -- gauche
		self.x = self.x - self.speed * dt
	elseif self.r == math.rad(0) then -- bas
		self.y = self.y + self.speed * dt
	elseif self.r == math.rad(180) then -- haut
		self.y = self.y - self.speed * dt
	end
end

function Arrow:draw()
	-- On dessine la flèche
	love.graphics.draw(self.img, self.x, self.y, self.r)
end
