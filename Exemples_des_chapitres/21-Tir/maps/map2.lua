local map2 = {}

map2.grid = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,17,1,1,1,1,1,1,1,1,1,1,1,4,3,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,2,1,1,3,1,1,1,1,1,2,1,1,1,1,1,1,1,1,19,20,20,20,21,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,4,1,1,1,1,1,3,1,1,4,1,1,22,8,8,8,22,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,22,8,8,8,22,4,1,1,1,1,1,2,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,22,8,8,8,22,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,22,8,8,8,22,1,1,1,1,1,1,1,1,1,1,1,1,1,14,14,14,14,14,14,14,14,14,14,20,20,20,20,20,8,8,8,20,20,20,20,20,20,20,20,20,1,3,1,1,14,2,2,2,1,3,1,1,3,1,3,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,1,3,1,1,14,2,16,2,1,1,1,3,1,1,3,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,1,1,1,1,14,2,2,2,1,3,1,1,1,3,3,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,1,1,1,1,1,14,14,14,14,14,14,14,14,14,14,24,24,24,24,21,8,8,8,24,24,24,24,24,24,24,24,24,1,4,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,21,8,8,8,22,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,22,8,8,8,22,1,1,1,2,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,22,8,8,8,22,1,1,1,1,1,1,2,1,1,1,4,1,1,1,1,2,1,1,1,1,1,2,1,1,4,1,1,22,8,8,8,22,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,23,24,24,24,25,1,1,1,4,3,1,1,1,1,1,4,1,1,1,1,1,1,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11}

arrowsMob = {}
arrowMobR = math.rad(-90)

function map2.draw()
	love.graphics.draw(mob1.sheet, mob1.sprite, mob1.x, mob1.y)	
	for i, v in ipairs(arrowsMob) do 
		v:draw()
	end
end

function map2.update(dt)
	-- Si le player touche la zone pour aller à map1
	if CheckCollision(player.posX, player.posY, tileSize, tileSize,
		31*tileSize, 11*tileSize, tileSize*1, tileSize*3) then
		-- On stoppe le mouvement actuel
		tween:stop()
		-- On positionne le player de l'autre côté de l'écran
		player.posX = 1*tileSize
		-- On change la map
		mapActuelle = "Map1"
	end

	for i, v in ipairs(arrowsMob) do
		v:update(dt)
		if(CheckCollision(player.posX, player.posY, tileSize, tileSize, v.x, v.y, 5, 5)) then
			print("Joueur touché par une flèche !!!")
		end
	end

	EnnemyAi(dt, mob1) -- IA de l'ennemi
end

function EnnemyAi(dt, mob)
	-- Gérer le mouvement du mob
	if mob.goRight then
		mob.x = mob.x + mob.speed * dt
		mob.sprite:setViewport(0, 2*tileSize, tileSize, tileSize)
	else
		mob.x = mob.x - mob.speed * dt
		mob.sprite:setViewport(0, tileSize, tileSize, tileSize)
	end

	if mob.x > 18*tileSize then
		mob.goRight = false
	elseif mob.x < 8*tileSize then
		mob.goRight = true
	end 

	-- Gérer la détection du joueur
	if mob.y-16 < player.posY and mob.y+16 > player.posY then
		-- Si le joueur est à peu près sur la même ligne que le mob
		if Distance(mob.x, mob.y, player.posX, player.posY) < mob.detectDistance then
			-- Si le joueur est à portée
			print("Joueur vu par le mob !!!!!")
			if love.math.random( 0, 1000 ) > 990 then
				table.insert(arrowsMob, Arrow(mob.x, mob.y, math.rad(-90)))
			end
		end
	end 

	-- Gérer la collision entre mob et joueur
	if CheckCollision(player.posX, player.posY, tileSize/2, tileSize/2,
		mob.x, mob.y, tileSize/2, tileSize/2) then
		-- Petite zone de collision pour moins de difficulté
		print("Joueur touché !")
	end

end

return map2
