-- Pour travailler avec le module socket
local socket = require('socket')
-- URL & port
local address, port = "localhost", 12345
-- Création de notre var udp
udp = socket.udp()
udp:setpeername(address, port)
udp:settimeout(0)


require "map" 
flux = require "flux" 
tileset = love.graphics.newImage("tileset.png")
quads = {} 
player = {}
player.posX = 128
player.posY = 352
player.speed = 32
player.sprite_sheet = love.graphics.newImage("hero.png")
player.yline = 32
player.xline = 0
player.sprite = love.graphics.newQuad(player.xline, player.yline, tileSize, tileSize, player.sprite_sheet:getDimensions())
player.anim_timer = 0.1
player.frame = 0
player.max_frame = 2

function love.load()
	for i=0, 41 do
		table.insert(quads, love.graphics.newQuad(i * tileSize, 0, tileSize, tileSize, tileset:getDimensions()))
	end
end

function love.draw()
	DrawMap(map1)
	love.graphics.draw(player.sprite_sheet, player.sprite, player.posX, player.posY)
end	

function love.update(dt)
	flux.update(dt) 
	InputManager(dt) 
	AnimPlayer(dt) 

	-- Envoyer la position du joueur au serveur
	-- En passant par udp et notre socket
	-- On envoie posX et posY sous forme de string type '50-50'
	udp:send(tostring(player.posX)..'-'..tostring(player.posY)
		..'-'..tostring(player.xline)..'-'..tostring(player.yline))

end

function DrawMap(map)
	cpt = 1 
	for y=1, love.graphics.getHeight(), tileSize do
		for x=1, love.graphics.getWidth(), tileSize do
			love.graphics.draw(tileset, quads[map[cpt]], x, y)
			cpt = cpt + 1
		end
	end
end

function AnimPlayer(dt)
	player.anim_timer = player.anim_timer - dt
	if player.anim_timer <= 0 then
		player.anim_timer = 0.1
		player.frame = player.frame + 1
		if player.frame > player.max_frame then player.frame = 0 end
		player.xline = tileSize * player.frame
		player.sprite:setViewport(player.xline, player.yline, tileSize, tileSize)
	end
end

function InputManager(dt)
	if love.keyboard.isDown("up") then
		MovePlayer(player.posX, player.posY - player.speed, 3)
	elseif love.keyboard.isDown("down") then
		MovePlayer(player.posX, player.posY + player.speed, 0)
	elseif love.keyboard.isDown("left") then
		MovePlayer(player.posX - player.speed, player.posY, 1)
	elseif love.keyboard.isDown("right") then
		MovePlayer(player.posX + player.speed, player.posY, 2)
	else
		player.max_frame = 0 
	end
end

function MovePlayer(x,y,yline)
	player.yline = tileSize * yline 
	player.max_frame = 2 
	caseX = math.floor((x/tileSize)+1)
    caseY = math.floor((y/tileSize)+1)
    flux.to(player, 0.3, { posX = x, posY = y }) 
end