# Livre Initiation à la création de jeux vidéo en Lua avec Löve2D

Vous trouverez sur ce dépôt les ressources liées au livre [Initiation à la création de jeux vidéo en Lua avec Löve2D](https://www.d-booker.fr/luajvlove2d/654-initiation-a-la-creation-de-jeux-video-en-lua-avec-love2d.html), écrit par Anthony Cardinale (parution le 24/09/2020).

![Couverture du livre](livre_lua_love2d_jv.png)

## Prérequis

Pour travailler avec ces sources, il vous faudra un éditeur de code type SublimeText afin de lire le Lua dans de bonnes conditions (coloration syntaxique).
Pour lancer les différents projets, il vous faudra LÖVE (Dans ce livre j'utilise la version 11.3). Vous pourrez alors faire glisser/déposer les projets sur l'exécutable de LÖVE.

## Arborescence

Le dossier Editeur de niveaux contient les sources de l'éditeur de maps que l'on développe dans ce livre.
Le dossier Exemples des chapitres contient le code source de tous les chapitres.
Le sous-dossier Exemples des chapitres/0-Exercices contient la correction des exercices pratiques.



Si ces exemples vous intéressent et que vous n'avez pas acheté le livre, nous vous invitons à le [faire](https://www.d-booker.fr/luajvlove2d/654-initiation-a-la-creation-de-jeux-video-en-lua-avec-love2d.html). Sa rédaction est le fruit d'un gros travail et votre soutien nous sera précieux.

